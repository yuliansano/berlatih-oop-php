<?php
    require_once('animal.php');
    require_once('frog.php');
    require_once('ape.php');

    echo "<br><br>";
    echo "<br><br>";

    $sheep = new Animal("shaun");

    echo "Name : " . $sheep->name."<br>"; 
    echo "Legs : " . $sheep->legs."<br>"; 
    echo "Cold Bloded : " . $sheep->cold_blooded."<br><br>"; 

    $kodok = new Frog("buduk");
    echo "Name : " . $kodok->name."<br>"; 
    echo "Legs : " . $kodok->legs."<br>"; 
    echo "Cold Bloded : " . $kodok->cold_blooded."<br>"; 
    echo $kodok->jump() ."<br><br>";

    $sungokong = new Ape("kera sakti");
    echo "Name : " . $sungokong->name."<br>"; 
    echo "Legs : " . $sungokong->legs."<br>"; 
    echo "Cold Bloded : " . $sungokong->cold_blooded."<br>";
    echo $sungokong->yell(); 
?> 